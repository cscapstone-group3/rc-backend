package com.example.vtresearchconnect.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.vtresearchconnect.DTO.CommentDTO;
import com.example.vtresearchconnect.DTO.DiscussionDTO;
import com.example.vtresearchconnect.DTO.StudentDTO;
import com.example.vtresearchconnect.Service.DiscussionService;
import com.example.vtresearchconnect.Service.EmailService;
import com.example.vtresearchconnect.Service.StudentService;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/discussions")
public class DiscussionController {

    @Autowired
    private DiscussionService discussionService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private EmailService emailService;

    @GetMapping
    public List<DiscussionDTO> getAllDiscussionsWithComments() {
        List<DiscussionDTO> discussions = discussionService.getAllDiscussionsWithComments();
        return discussions.stream()
            .sorted((p1, p2) -> p2.getCreatedAt().compareTo(p1.getCreatedAt()))
            .collect(Collectors.toList());
    }

    @GetMapping("/id/{id}")
    public DiscussionDTO getDiscussionById(@PathVariable long id) {
        return discussionService.getDiscussionById(id);
    }

    @PostMapping("/add")
    public ResponseEntity<Void> addDiscussion(@RequestBody DiscussionDTO discussionDTO) {
        try{
            discussionService.addDiscussion(discussionDTO); // Assuming you have a method in your service layer to add a discussion
        }
        catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }

        String labName = discussionDTO.getLabName();

        List<StudentDTO> students = studentService.getAllStudents();
        for(StudentDTO student: students){
            if(student.getSubscriptions().contains(labName) && student.getId() != discussionDTO.getStudent().getId()){
                //String emailBody = "<p> " + labName + " had a discussion created by " + discussionDTO.getStudent().getFirstName() + " " + discussionDTO.getStudent().getLastName()+ "!</p>";
                String emailBody = "<html>" +
                        "<body style=\"font-family: Arial, sans-serif;\">" +
                        "<div style=\"max-width: 600px; margin: 0 auto; padding: 20px; border: 1px solid #e0e0e0; border-radius: 10px; background-color: #f9f9f9;\">" +
                        "<h2 style=\"color: #333; text-align: center;\">New Discussion Created</h2>" +
                        "<p>A discussion was created in <strong>" + labName + "</strong> by " + discussionDTO.getStudent().getFirstName() + " " + discussionDTO.getStudent().getLastName() + "!</p>" +
                        "<p>We thought you would be interested in joining the discussion. Please visit the VT Research Connect platform to view and participate in the discussion.</p>" +
                        "<p>Thank you for being a part of our community!</p>" +
                        "<p>Best regards,</p>" +
                        "<p>The VT Research Connect Team</p>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
                emailService.sendEmail(
                    student.getEmail().toLowerCase(),
                    "VT Research Connect Subscription Notification: Discussion",
                    emailBody
                );
            }
        }


        return ResponseEntity.ok().build();
    }

    @PostMapping("/addComment/{discussionId}")
    public ResponseEntity<Void> addComment(@PathVariable long discussionId, @RequestBody CommentDTO commentDTO) {
        discussionService.addComment(commentDTO, discussionId);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDiscussion(@PathVariable long id) {
        try {
            discussionService.deleteDiscussion(id);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/comments/{commentId}")
    public ResponseEntity<Void> deleteComment(@PathVariable long commentId) {
        try {
            discussionService.deleteComment(commentId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}